# NewFortress #

추억의 fortress 게임을 html, javascript, css, jquery를 이용하여 구현하였습니다.

### Mode ###

1vs1 모드와 vs com 모드가 있으나
vs com 모드는 준비중입니다. ㅠㅠ

### 사용방법 ###

소스를 다운받으신 후 fortress 폴더, bgm 폴더, css 폴더, img 폴더, js 폴더 를
하나의 폴더에 넣어주시고 fortress 폴더의 load.html을 실행시키시면 됩니다.

### 조작방법 ###

notice.html에서 설명해 놓았습니다.