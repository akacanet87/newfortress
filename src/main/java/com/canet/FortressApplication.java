package com.canet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

@Configuration
@SpringBootApplication
public class FortressApplication {

	public static void main(String[] args) {
		SpringApplication.run(FortressApplication.class, args);
	}
}
