/* bgm sources */
var loadBgm = '../bgm/load.mp3';
var mode1Bgm = '../bgm/mode1.mp3';
var mode2Bgm = '../bgm/mode2.mp3';
var noticeBgm = '../bgm/notice.mp3';
var overBgm = '../bgm/over.mp3';
var readyBgm = '../bgm/ready.mp3';
var roomBgm = '../bgm/room.mp3';
var shopBgm = '../bgm/shop.mp3';

var castleBgm = '../bgm/castle.mp3';
var desertBgm = '../bgm/desert.mp3';
var extinctionBgm = '../bgm/extinction.mp3';
var forestBgm = '../bgm/forest.mp3';
var nightBgm = '../bgm/night.mp3';
var ruinedCityBgm = '../bgm/ruinedCity.mp3';
var skyBgm = '../bgm/sky.mp3';

var shotCannonBgm = '../bgm/shotCannon.mp3';
var shotDukeBgm = '../bgm/shotDuke.mp3';
var shotMissileBgm = '../bgm/shotMissile.mp3';
var shotOctopusBgm = '../bgm/shotOctopus.mp3';
var shotStarBgm = '../bgm/shotStar.mp3';

var explodeCannonBgm = '../bgm/explodeCannon.mp3';
var explodeDukeBgm = '../bgm/explodeDuke.mp3';
var explodeMissileBgm = '../bgm/explodeMissile.mp3';
var explodeOctopusBgm = '../bgm/explodeOctopus.mp3';
var explodeStarBgm = '../bgm/explodeStar.mp3';

var moveCannonBgm = '../bgm/moveCannon.mp3';
var moveDukeBgm = '../bgm/moveDuke.mp3';
var moveMissileBgm = '../bgm/moveMissile.mp3';
var moveOctopusBgm = '../bgm/moveOctopus.mp3';
var moveStarBgm = '../bgm/moveStar.mp3';

var stopCannonBgm = '../bgm/stopCannon.mp3';
var stopDukeBgm = '../bgm/stopDuke.mp3';
var stopMissileBgm = '../bgm/stopMissile.mp3';
var stopOctopusBgm = '../bgm/stopOctopus.mp3';
var stopStarBgm = '../bgm/stopStar.mp3';

var clickBgm = '../bgm/click.mp3';
var hoverBgm = '../bgm/hover.mp3';

var deadBgm = '../bgm/dead.mp3';
var winBgm = '../bgm/win.mp3';
var helicopterBgm = '../bgm/helicopter.mp3';
var turnDownBgm = '../bgm/turn.mp3';
var warningBgm = '../bgm/warning.mp3';

/* 최초 window load 시 화면 크기를 지정함 (높이에 따라 넓이를 지정) */
window.addEventListener('load', function () {

    console.log(parseInt($('.wrapper').css('height')));
    $('.wrapper').css('width', (parseInt($('.wrapper').css('height')) * 16 / 9 ) + 'px');

});

/* 화면 크기 변경 이벤트 발생 시 화면 크기를 지정함 (높이에 따라 넓이를 지정) */
window.addEventListener('resize', function () {

    console.log(parseInt($('.wrapper').css('height')));
    $('.wrapper').css('width', (parseInt($('.wrapper').css('height')) * 16 / 9 ) + 'px');

});

$(document).ready(function () {

    /* 새로고침 키 막기 */
    /* 빈 div 에 포커스를 주어 창이 갖고 있던 포커스 뺏어옴 */
    $('#empty-div').focus();
    // To disable f5
    /* jQuery < 1.7 */
    $(document).bind("keydown", disableF5);
    /* OR jQuery >= 1.7 */
    $(document).on("keydown", disableF5);

    var hoverEffect = new effectBgm(hoverBgm, false);
    var clickEffect = new effectBgm(clickBgm, false);

    /* 버튼에 hover 또는 클릭 시 효과음 */
    $('button[type=button]').hover(function () {
        hoverEffect.play();
    }).on('mousedown', function () {
        clickEffect.stop();
        clickEffect.play();
    });

    /* id가 btn-close 라면 클릭 시 종료 확인 창 */
    $('#btn-close').on('click', function () {
        if (confirm('정말 종료하시겠습니까?')) {
            sessionStorage.clear();
            window.close();
        }
    });

    /* id가 btn-notice 라면 클릭 시 notice.html로 이동 */
    $('#btn-notice').on('click', function () {
        location.href = '../fortress/notice.html';
    });

    /* id가 btn-mode 라면 클릭 시 session에 저장되어 있던 값 모두 삭제 후 mode.html로 이동 */
    $('#btn-mode').on('click', function () {
        sessionStorage.clear();
        location.href = '../fortress/mode.html';
    });

    /* single mode 버튼 클릭 시 single mode 상태로 room.html 에 이동 (구현 안되어 alert 창으로 안내) */
    $('#btn-single').on('click', function () {
        alert('준비중이에요.. ㅠㅠ')
        /*sessionStorage.setItem('mode', 'single');
         location.href = '../room.html';*/
    });

    /* multi mode 버튼 클릭 시 multi mode 상태로 room.html 에 이동 */
    $('#btn-multi').on('click', function () {
        sessionStorage.setItem('mode', 'multi');
        location.href = '../fortress/room.html';
    });

});

// slight update to account for browsers not supporting e.which
function disableF5(e) {
    if ((e.which || e.keyCode) === 116) e.preventDefault();
}

/*// To re-enable f5
 /!* jQuery < 1.7 *!/
 $(document).unbind("keydown", disableF5);
 /!* OR jQuery >= 1.7 *!/
 $(document).off("keydown", disableF5);*/

/* 진행 상태 바를 임의로 넣어주고 완료후 page로 이동 */
function setProgress(page) {

    var $progressBar = $('.prog-bar');
    var $progressValue = $('.prog-val');
    var width = 1;
    var val = 0;
    var st = null;

    frame();

    function frame() {

        var val = parseInt(Math.random() * 100) + 20;
        st = null;
        if (width >= 100) {
            if (location !== null && location !== undefined && location !== '') {
                location.href = page;
            }
        } else {
            width++;
            $progressValue.text(width + '%');
            $progressBar.css('width', width + '%');
            st = setTimeout(function () {
                frame();
            }, val);
        }

    }

}

/* bgm 재생 */
function playBgm(src) {

    var audio = new Audio(src);

    audio.play();
    audio.loop = true;
    audio.volume = 0.2;

}

/* 효과음 재생 */
function effectBgm(src, loop) {

    this.audio = new Audio(src);
    this.audio.loop = loop;
    this.audio.preload = 'auto';
    this.audio.volume = 0.15;

    this.play = function () {
        this.audio.play();
    };

    this.stop = function () {
        this.audio.pause();
        this.audio.currentTime = 0;
    }

}