var mapSt;                  //  map 이동 관련 setTimeout
var turnSt;                 //  turn 관련 setTimeout
var turnTime = 32;          //  최초 turn 시간은 32초에서 시작
var turnCount = 0;          //  turnCount를 1초마다 올려서 32초부터 감소
var turnInterval = 1000;    //  turn 간격은 1초

var isFirst = true;         //  게임 시작 후 최초의 턴인지를 알려주는 flag
var isFired = false;        //  총알이 발사된 상태인지를 알려주는 flag
var isMoving = false;       //  탱크가 움직이고 있는 상태인지를 알려주는 flag
var isShooting = false;     //  power 게이지를 조작 중인 상태인지를 알려주는 flag

var isLeftSide = false;     //  map 움직이기 위해 왼쪽 편에 있는 div에 올라와 있는지를 알려주는 flag
var isRightSide = false;    //  map 움직이기 위해 오른쪽 편에 있는 div에 올라와 있는지를 알려주는 flag
var isTopSide = false;      //  map 움직이기 위해 위쪽 편에 있는 div에 올라와 있는지를 알려주는 flag
var isBottomSide = false;   //  map 움직이기 위해 아래쪽 편에 있는 div에 올라와 있는지를 알려주는 flag

var mode;                   //  single, multi mode를 구분하기 위함
var tank1pStat;             //  1p 탱크의 능력치
var tank2pStat;             //  2p 탱크의 능력치
var item1p;                 //  1p item
var item2p;                 //  2p item
var mapStat;                //  map의 상태

var tank1player = null;     //  1p의 상세 정보
var tank2player = null;     //  2p의 상세 정보

var is1pTurn = true;        //  턴 안내 flag

var isMapMoving = false;    //  맵이 움직이고 있는 상태인지를 알려주는 flag

/* 나머지 game의 기본 상태들 */
var mapWidth = 2100;
var mapHeight = 1400;
var moveMapPx = 10;
var moveMapInterval = 20;
var gravity = 0.049;
var wind = 0;
var moveTankInterval = 10;
var blockArr = [];
var blockSize = 20;
var destroyCount = 0;
var roundCount = 0;
var winnerBgm = null;
var loserBgm = null;

$(document).ready(function () {

    initGame();
    initMap();
    setMapBar();
    initPlayer();
    initCanvas();
    initItem();
    setInterval(function () {
        var windUnit = new initWind();
        windUnit.init();
    }, 2000);
    playBgm(mapStat.bgm);
    gameStart();

    /* keyup 되었을 때의 event 관리 */
    $(window).on('keyup', function (event) {

        switch (event.keyCode) {
            case 37:
                if (!isShooting && !isFired) {
                    $('#map').stop();
                    isMoving = false;
                    moveTank(0, false);
                }
                break;
            case 39:
                if (!isShooting && !isFired) {
                    $('#map').stop();
                    isMoving = false;
                    moveTank(0, true);
                }
                break;
            case 38:
                break;
            case 40:
                break;
            case 32:
                if (!isFired) {
                    clearInterval(turnSt);
                    turnSt = null;
                    isShooting = false;
                    tank1player.stop();
                    tank2player.stop();
                    var bullet = null;
                    if (is1pTurn) {
                        bullet = new shotBullet(tank1player);
                    } else {
                        bullet = new shotBullet(tank2player);
                    }
                    bullet.ready();
                    bullet.fire();
                }
                break;

        }

    });

    /* keydown 되었을 때의 event 관리 */
    $(window).on('keydown', function (event) {

        switch (event.keyCode) {
            case 37:
                if (!isShooting && !isFired) {
                    moveTank(-1, false);
                    setDegreeGauge(0);
                }
                break;
            case 39:
                if (!isShooting && !isFired) {
                    moveTank(1, true);
                    setDegreeGauge(0);
                }
                break;
            case 38:
                if (!isShooting && !isFired) {
                    setDegreeGauge(1);
                }
                break;
            case 40:
                if (!isShooting && !isFired) {
                    setDegreeGauge(-1);
                }
                break;
            case 32:
                if (!isMoving && !isFired) {
                    isShooting = true;
                    setPowerGauge(1);
                }
        }

    });

    /* item 클릭했을 경우 item 효과를 각 player에 세팅 */
    $('#status-1p-item1').on('click', function () {
        if (is1pTurn) {
            console.log('1p item1 클릭');
            checkItem($('#status-1p-item1'), tank1player);
        }
    });
    $('#status-1p-item2').on('click', function () {
        if (is1pTurn) {
            console.log('1p item2 클릭');
            checkItem($('#status-1p-item2'), tank1player);
        }
    });
    $('#status-1p-item3').on('click', function () {
        if (is1pTurn) {
            console.log('1p item3 클릭');
            checkItem($('#status-1p-item3'), tank1player);
        }
    });
    $('#status-1p-item4').on('click', function () {
        if (is1pTurn) {
            console.log('1p item4 클릭');
            checkItem($('#status-1p-item4'), tank1player);
        }
    });
    $('#status-2p-item1').on('click', function () {
        if (!is1pTurn) {
            console.log('2p item1 클릭');
            checkItem($('#status-2p-item1'), tank2player);
        }
    });
    $('#status-2p-item2').on('click', function () {
        if (!is1pTurn) {
            console.log('2p item2 클릭');
            checkItem($('#status-2p-item2'), tank2player);
        }
    });
    $('#status-2p-item3').on('click', function () {
        if (!is1pTurn) {
            console.log('2p item3 클릭');
            checkItem($('#status-2p-item3'), tank2player);
        }
    });
    $('#status-2p-item4').on('click', function () {
        if (!is1pTurn) {
            console.log('2p item4 클릭');
            checkItem($('#status-2p-item4'), tank2player);
        }
    });
});

/* session으로부터 저장되어 있는 값들을 가져와 각각에 세팅해준다 */
function initGame() {

    mode = sessionStorage.getItem('mode');
    tank1pStat = JSON.parse(sessionStorage.getItem('tank1pStat'));
    tank2pStat = JSON.parse(sessionStorage.getItem('tank2pStat'));
    item1p = JSON.parse(sessionStorage.getItem('item1p'));
    item2p = JSON.parse(sessionStorage.getItem('item2p'));
    mapStat = JSON.parse(sessionStorage.getItem('mapStat'));

    console.log(mode);
    console.log(tank1pStat);
    console.log(tank2pStat);
    console.log(item1p);
    console.log(item2p);
    console.log(mapStat);

    if (mode === 'single') {
        console.log('싱글임');
    } else {
        console.log('멀티임');
    }

    if (tank1pStat !== null) {
        console.log(tank1pStat);
    } else {
        tank1pStat = tankCannonStat;
    }

    if (tank2pStat !== null) {
        console.log(tank2pStat);
    } else {
        tank2pStat = tankMissileStat;
    }

    if (mapStat !== null) {
        console.log(mapStat);
    } else {
        mapStat = mapCastle;
    }

}

/* map과 각 map이 보유한 block 정보를 세팅 */
function initMap() {

    $('#map').css('background-image', 'url(\'' + mapStat.img + '\')');
    for (var i = 0; i < mapStat.areaX.length; i++) {
        initBlock(mapStat.block, mapStat.areaX[i], mapStat.areaY[i], mapStat.blockLen[i]);
    }

}

/* map 움직임 세팅 */
function setMapBar() {

    $('#side-left').hover(function () {
        //console.log('side-left-in');
        isMapMoving = true;
        isLeftSide = true;
        mapSt = setInterval(function () {
            moveMap()
        }, moveMapInterval);
    }, function () {
        //console.log('side-left-out');
        isMapMoving = false;
        isLeftSide = false;
        clearInterval(mapSt);
        mapSt = null;
    });
    $('#side-right').hover(function () {
        //console.log('side-right-in');
        isMapMoving = true;
        isRightSide = true;
        mapSt = setInterval(function () {
            moveMap()
        }, moveMapInterval);
    }, function () {
        //console.log('side-right-out');
        isMapMoving = false;
        isRightSide = false;
        clearInterval(mapSt);
        mapSt = null;
    });
    $('#side-top').hover(function () {
        //console.log('side-top-in');
        isMapMoving = true;
        isTopSide = true;
        mapSt = setInterval(function () {
            moveMap()
        }, moveMapInterval);
    }, function () {
        //console.log('side-top-out');
        isMapMoving = false;
        isTopSide = false;
        clearInterval(mapSt);
        mapSt = null;
    });
    $('#side-bottom').hover(function () {
        //console.log('side-bottom-in');
        isMapMoving = true;
        isBottomSide = true;
        mapSt = setInterval(function () {
            moveMap()
        }, moveMapInterval);
    }, function () {
        //console.log('side-bottom-out');
        isMapMoving = false;
        isBottomSide = false;
        clearInterval(mapSt);
        mapSt = null;
    });

}

/* 각 side의 불투명 노란 영역에 마우스 올릴 시에 맵 이동 */
function moveMap() {

    if (isLeftSide) {
        if (parseInt($('#map').css('left')) >= 0) {
            $('#map').css('left', 0 + 'px');
            isLeftSide = false;
            clearInterval(mapSt);
            mapSt = null;
        } else {
            $('#map').css('left', ( parseInt($('#map').css('left')) + moveMapPx ) + 'px');
        }
        //console.log($('#map').css('left'));
    }

    if (isRightSide) {
        if (parseInt($('#map').css('left')) < parseInt($('.wrapper').css('width')) - mapWidth) {
            $('#map').css('left', (parseInt($('.wrapper').css('width')) - mapWidth) + 'px');
            isRightSide = false;
            clearInterval(mapSt);
            mapSt = null;
        } else {
            $('#map').css('left', ( parseInt($('#map').css('left')) - moveMapPx ) + 'px');
        }
        //console.log($('#map').css('left'));
    }

    if (isTopSide) {
        if (parseInt($('#map').css('top')) >= 0) {
            $('#map').css('top', 0 + 'px');
            isTopSide = false;
            clearInterval(mapSt);
            mapSt = null;
        } else {
            $('#map').css('top', ( parseInt($('#map').css('top')) + moveMapPx ) + 'px');
        }
        //console.log($('#map').css('top'));
    }

    if (isBottomSide) {
        if (parseInt($('#map').css('top')) < parseInt($('.wrapper').css('height')) - mapHeight) {
            $('#map').css('top', (parseInt($('.wrapper').css('height')) - mapHeight) + 'px');
            isBottomSide = false;
            clearInterval(mapSt);
            mapSt = null;
        } else {
            $('#map').css('top', ( parseInt($('#map').css('top')) - moveMapPx ) + 'px');
        }
        //console.log($('#map').css('top'));
    }

}

/* block 세팅 */
function initBlock(img, x, y, len) {

    var blockImg = $('#block').clone();
    blockImg.css({
        'background-image': 'url(\'' + img + '\')',
        'background-repeat': 'repeat',
        'width': blockSize * len + 'px',
        'height': blockSize * 2 + 'px',
        'left': x + 'px',
        'top': y + 'px',
        'display': 'block'
    });

    $('#map').append(blockImg);
    blockArr.push(blockImg);

    /*var blockImg = $('#map').append(
     "<div style=\"background-image:url(" + img + "); " +
     "background-repeat:repeat; " +
     "width:" + blockSize * len + "px; " +
     "height:" + blockSize * 2 + "px; " +
     "left:" + x + "px; " +
     "top:" + y + "px; " +
     "position: absolute;" +
     "\"></div>");

     blockArr.push(blockImg);*/

    /*var blockImg = document.createElement("div");
     blockImg.style.backgroundImage = "url(\'" + img + "\')";
     blockImg.style.backgroundRepeat = "repeat";
     blockImg.style.width = blockSize * len + "px";
     blockImg.style.height = blockSize * 2 + "px";
     blockImg.style.position = "absolute";
     blockImg.style.left = x + "px";
     blockImg.style.top = y + "px";
     $('#map').append(blockImg);
     blockArr.push(blockImg);*/

}

/* 바람 이미지 세팅 */
function initWind() {

    this.wind = $('#wind').clone();
    this.img = mapStat.wind;
    this.posX = Math.random() * 2100;
    this.posY = 0;
    this.windSt = null;
    this.rotate = 0;
    this.deg = Math.random() * 3 + 3;

    this.init = function () {

        var me = this;

        this.wind.addClass(this.img + ' wind-unit');
        this.wind.css({
            'display': 'block',
            'left': this.posX + 'px',
            'top': this.posY + 'px'
        });

        $('#map').append(this.wind);

        this.windSt = setInterval(function () {
            me.fall();
        }, 10);

    };

    this.fall = function () {

        var me = this;

        this.posX += wind * 10;
        this.posY += 1;
        this.rotate += this.deg;

        this.wind.css({
            'left': this.posX + 'px',
            'top': this.posY + 'px',
            'transform': 'rotate(' + this.rotate + 'deg)'
        });

        // 부딪히지 않고 화면 밖으로 나가면
        if (( parseInt(this.wind.css('left')) > parseInt($('#map').css('width')) + 200 ) || ( parseInt(this.wind.css('left')) < -200 ) || ( parseInt(this.wind.css('top')) > ( parseInt($('#map').css('height')) + 200 ) )) {
            me.destroy();
        }

    };

    this.destroy = function () {

        this.wind.remove();
        clearInterval(this.windSt);
        this.windSt = null;

    };

}

/* 각 플레이어들의 각도 조절 창을 canvas로 세팅 */
function initCanvas() {

    var $canvas1 = $('#status-1p-degree').get(0);
    var $canvas2 = $('#status-2p-degree').get(0);
    var ctx1 = $canvas1.getContext('2d');
    var ctx2 = $canvas2.getContext('2d');
    var can1Width = parseInt($canvas1.width);
    var can1Height = parseInt($canvas1.height);
    var can2Width = parseInt($canvas2.width);
    var can2Height = parseInt($canvas2.height);

    ctx1.clearRect(0, 0, can1Width, can1Height);
    ctx1.beginPath();
    ctx1.moveTo(50, can1Height / 2);
    ctx1.lineTo(can1Width - 50, can1Height / 2);
    ctx1.strokeStyle = "#B5590E";
    ctx1.stroke();

    ctx2.clearRect(0, 0, can2Width, can2Height);
    ctx2.beginPath();
    ctx2.moveTo(50, can2Height / 2);
    ctx2.lineTo(can2Width - 50, can2Height / 2);
    ctx2.strokeStyle = "#B5590E";
    ctx2.stroke();

}

/* 초기 각 플레이어들이 생성되는 위치 세팅 */
function initPlayer() {

    var posX1;
    var posX2;
    loserBgm = new effectBgm(deadBgm, false);
    winnerBgm = new effectBgm(winBgm, false);
    //var isNear = true;

    /*while (isNear) {

     posX1 = parseInt(Math.random() * 700);
     posX2 = parseInt(Math.random() * 700);

     if (Math.abs(posX1 - posX2) >= 200) {
     isNear = false;
     break;
     }

     }*/

    posX1 = parseInt(Math.random() * 2000 + 50);
    posX2 = parseInt(Math.random() * 2000 + 50);

    tank1player = new initTank($('#tank-1p'), tank1pStat);
    tank2player = new initTank($('#tank-2p'), tank2pStat);

    tank1player.init();
    tank2player.init();
    tank1player.posX = posX1;
    tank2player.posX = posX2;

}

/* session으로부터 받아온 item들을 각 player들에게 할당하여 item 박스에 보여줌 */
function initItem() {

    if (item1p !== null) {
        console.log('item1p 존재');
        console.log(item1p[0]);
        console.log(item1p[1]);
        console.log(item1p[2]);
        console.log(item1p[3]);
        $('#status-1p-item1').addClass(item1p[0]);
        $('#status-1p-item2').addClass(item1p[1]);
        $('#status-1p-item3').addClass(item1p[2]);
        $('#status-1p-item4').addClass(item1p[3]);
    } else {
        $('#status-1p-item1').addClass('box-panel');
        $('#status-1p-item2').addClass('box-panel');
        $('#status-1p-item3').addClass('box-panel');
        $('#status-1p-item4').addClass('box-panel');
    }

    if (item2p !== null) {
        console.log('item2p 존재');
        console.log(item2p[0]);
        console.log(item2p[1]);
        console.log(item2p[2]);
        console.log(item2p[3]);
        $('#status-2p-item1').addClass(item2p[0]);
        $('#status-2p-item2').addClass(item2p[1]);
        $('#status-2p-item3').addClass(item2p[2]);
        $('#status-2p-item4').addClass(item2p[3]);
    } else {
        $('#status-2p-item1').addClass('box-panel');
        $('#status-2p-item2').addClass('box-panel');
        $('#status-2p-item3').addClass('box-panel');
        $('#status-2p-item4').addClass('box-panel');
    }

}

/* game 시작 시 각 플레이어들에게 포커스 주기 */
function gameStart() {

    var count = 0;
    var readySt = setInterval(function () {
        if (tank1player.isInit && tank2player.isInit) {
            if (count < 4) {
                if (count % 2 === 0) {
                    setAniFocus(tank1player.tank, 1200);
                } else {
                    setAniFocus(tank2player.tank, 1200);
                }
                count++;
            } else {
                clearInterval(readySt);
                tank1player.isReady = true;
                tank2player.isReady = true;
                makeTurn();
                console.log('게임 시작!');
            }
        }
    }, 1500);

}

/* turn과 관련된 함수, 매 턴마다 바람세기 조절 및 플레이어들의 hp 판단, turnCount 제어 */
function makeTurn() {

    $('.turn-unit').remove();
    clearInterval(turnSt);
    roundCount++;
    turnCount = turnTime;
    isFired = false;
    setWindGauge();
    if (isFirst) {
        is1pTurn = true;
        isFirst = false;
    } else {
        is1pTurn = !is1pTurn;
    }

    if (tank1player.hp <= 0 || tank2player.hp <= 0) {
        tank1player.stop();
        tank2player.stop();
        if (tank1player.hp <= 0) {
            sessionStorage.setItem('winner', '2p');
        } else if (tank2player.hp <= 0) {
            sessionStorage.setItem('winner', '1p');
        }

        setTimeout(function () {
            location.href = '../fortress/over.html'
        }, 5000);

    } else {

        var turnBgm = new effectBgm(turnDownBgm, false);
        var warnBgm = new effectBgm(warningBgm, false);

        var player = null;
        var $panel = null;

        if (is1pTurn) {
            console.log('1p 턴');
            player = tank1player;
            $panel = $('#status-1p-timer');
        } else {
            console.log('2p 턴');
            player = tank2player;
            $panel = $('#status-2p-timer');
        }

        player.reInit();

        turnSt = setInterval(function () {

            turnCount--;

            if (turnCount === 0) {
                $panel.text(turnCount);
                player.stop();
            } else if (turnCount < 0) {
                if (turnCount === -2) {
                    makeTurn();
                }
            } else {
                $panel.text(turnCount);
                if (turnCount <= 5) {
                    warnBgm.play();
                } else {
                    turnBgm.play();
                }
            }

        }, turnInterval);

    }

}

/* 각 플레이어의 탱크 세팅, item 효과와 turn 이미지, 탱크의 움직임 제어 */
function initTank($tank, tankStat) {

    this.tank = $tank;
    this.img = tankStat.img;
    this.deadImg = tankStat.dead;
    this.orgHp = tankStat.hp;
    this.hp = tankStat.hp;
    this.damage = tankStat.damage;
    this.distance = tankStat.distance;
    this.bullet = tankStat.bullet;
    this.spark = tankStat.spark;
    this.moveBgm = null;
    this.stopBgm = null;
    this.shotBgm = null;
    this.explodeBgm = null;
    this.posX = 0;
    this.posY = 0;
    this.velX = 0;
    this.velY = 0;
    this.isInit = false;
    this.isReady = false;
    this.isFalling = true;
    this.isRight = true;
    this.tankSt = null;
    this.availDt = 0;
    this.isPwUp = true;
    this.shotDeg = 0;
    this.angleX = 0;
    this.angleY = 0;
    this.firePw = 0;
    this.isDouble = false;
    this.isPower = false;
    this.isLock = false;
    this.isLocked = false;
    this.turnImg = null;
    this.turnImgX = 0;
    this.lockCount = 0;

    this.init = function () {

        var me = this;
        this.moveBgm = new effectBgm(tankStat.move, true);
        this.stopBgm = new effectBgm(tankStat.stop, false);
        this.shotBgm = new effectBgm(tankStat.shot, false);
        this.explodeBgm = new effectBgm(tankStat.explode, false);
        this.tank.addClass(this.img);
        this.availDt = this.distance;
        //this.move();
        this.tankSt = setInterval(function () {
            me.move();
        }, moveTankInterval);

    };

    this.reInit = function () {

        var me = this;

        setAniFocus(this.tank, 'slow');
        setLastPwGauge(this.firePw);
        if (this.isLocked) {
            this.availDt = 0;
            if ((roundCount - this.lockCount) > 3) {
                this.isLocked = false;
            }
        } else {
            this.availDt = this.distance;
        }
        if (this.isLock) {
            this.isLock = false;
        }
        this.turnImg = $('#turn').clone();
        $('#map').append(this.turnImg);
        this.turnImg.addClass('turn-unit');
        this.turnImgX = ( parseInt(this.tank.css('width')) - parseInt(this.turnImg.css('width'))) / 2;
        this.turnImg.css('display', 'block');
        this.velX = 0;
        this.firePw = 0;
        this.isPwUp = true;
        //this.move();
        this.tankSt = setInterval(function () {
            me.move();
        }, moveTankInterval);

    };

    this.move = function () {

        var me = this;

        /*this.tankSt = setTimeout(function () {
         me.move();
         }, moveTankInterval);*/

        for (var i = 0; i < blockArr.length; i++) {
            if (isHit(this.tank, blockArr[i])) {
                this.isFalling = false;
                break;
            } else {
                this.isFalling = true;
            }
        }

        if (this.isFalling) {
            this.velY += gravity;
            this.velX = 0;
            if (this.isReady) {
                this.turnImg.css({
                    'left': (this.posX + this.turnImgX) + 'px',
                    'top': this.posY - 60 + 'px'
                });
                setFocus(this.tank);
            }
        } else {
            if (!this.isReady) {
                me.stop();
                this.isInit = true;
            } else {
                this.turnImg.css({
                    'left': (this.posX + this.turnImgX) + 'px',
                    'top': this.posY - 60 + 'px'
                });
            }
            this.velY = 0;
        }

        if (this.availDt <= 0) {
            me.stop();
        }

        if (parseInt(this.tank.css('top')) > ( parseInt($('#map').css('height')) + 200 )) {
            this.hp = -100;
            makeTurn();
            me.stop();
        }

        this.posX = this.posX + this.velX;
        this.posY = this.posY + this.velY;
        this.availDt -= Math.abs(this.velX);

        setDistanceGauge(this.distance, this.availDt);

        this.tank.css('left', this.posX + 'px');
        this.tank.css('top', this.posY + 'px');

    };

    this.stop = function () {
        this.moveBgm.stop();
        this.stopBgm.stop();
        //clearTimeout(this.tankSt);
        clearInterval(this.tankSt);
        this.tankSt = null;
    };

}

/* 탱크가 움직일 때 탱크가 보는 방향 및 효과음 제어 */
function moveTank(distance, isRight) {

    isMoving = true;
    var player = null;
    if (is1pTurn) {
        player = tank1player;
    } else {
        player = tank2player;
    }

    if (isRight) {
        player.isRight = true;
        player.tank.removeClass('look-left').addClass('look-right');
    } else {
        player.isRight = false;
        player.tank.removeClass('look-right').addClass('look-left');
    }

    if (player.isReady) {

        setFocus(player.tank);

        if (distance === 0) {
            isMoving = false;
            player.moveBgm.stop();
        } else {
            if (player.availDt > 0) {
                player.moveBgm.play();
            } else {
                player.stopBgm.stop();
                player.stopBgm.play();
            }
        }

        player.velX = distance;

    }

}

/* 바람 게이지 창 세팅 */
function setWindGauge() {

    var add = Math.random() * 0.015;
    var flag = parseInt(Math.random() * 10);

    $('#status-wind-gauge').css({'left': '', 'right': ''});

    if (flag >= 5) {
        wind += add;
    } else {
        wind -= add;
    }

    console.log(wind);

    if (wind > 0.04) {
        wind = 0.04;
    }
    if (wind < -0.04) {
        wind = -0.04;
    }
    if (wind >= 0) {
        $('#status-wind-gauge').css({'left': '50%', 'width': (wind * 1250) + '%'});
    } else {
        $('#status-wind-gauge').css({'right': '50%', 'width': Math.abs(wind * 1250) + '%'});
    }

}

/* 각도 조절창 제어 */
function setDegreeGauge(deg) {

    var player = null;
    var $canvas = null;
    var $panel = null;

    if (is1pTurn) {
        player = tank1player;
        $canvas = $('#status-1p-degree').get(0);
        $panel = $('#status-1p-degree-show');
    } else {
        player = tank2player;
        $canvas = $('#status-2p-degree').get(0);
        $panel = $('#status-2p-degree-show');
    }

    player.shotDeg += deg;

    if (player.shotDeg === -1) {
        player.shotDeg = 0;
    } else if (player.shotDeg === 91) {
        player.shotDeg = 90;
    }

    var theta = Math.PI * player.shotDeg / 180;
    var angleX = Math.cos(theta);
    var angleY = Math.sin(theta);

    if (player.isRight) {
        angleY = -angleY;
        $panel.text(player.shotDeg);
    } else {
        angleX = -angleX;
        angleY = -angleY;
        $panel.text(-player.shotDeg);
    }

    var ctx = $canvas.getContext('2d');
    var canWidth = parseInt($canvas.width);
    var canHeight = parseInt($canvas.height);

    ctx.clearRect(0, 0, canWidth, canHeight);
    ctx.beginPath();
    ctx.moveTo(50, canHeight / 2);
    ctx.lineTo(canWidth - 50, canHeight / 2);
    ctx.strokeStyle = "#B5590E";
    ctx.stroke();
    ctx.moveTo(canWidth / 2, canHeight / 2);
    ctx.lineTo(canWidth / 2 + angleX * canWidth / 3, canHeight / 2 + angleY * canHeight / 3);
    ctx.strokeStyle = "#FAFF7C";
    ctx.stroke();

    player.angleX = angleX;
    player.angleY = angleY;

}

/* 파워 게이지 제어 */
function setPowerGauge(power) {

    var player = null;

    if (is1pTurn) {
        player = tank1player;
    } else {
        player = tank2player;
    }

    if (player.isReady) {

        if (player.firePw === 0) {
            player.isPwUp = true;
        }

        if (player.firePw === 100) {
            player.isPwUp = false;
        }

        if (player.isPwUp) {
            player.firePw += power;
        } else {
            player.firePw -= power;
        }

        if (is1pTurn) {
            $('#status-1p-pw').css('width', player.firePw + '%');
        } else {
            $('#status-2p-pw').css('width', player.firePw + '%');
        }

    }

    //console.log(player.firePw);

}

/* 탱크의 체력 제어 */
function setHpGauge(damage, $target) {

    $target.hp -= damage;

    if ($target.hp <= 0) {
        $target.hp = 0;
        $target.tank.removeClass().addClass('game-unit ' + $target.deadImg);
        if ($target.isRight) {
            $target.tank.addClass('look-right');
        } else {
            $target.tank.addClass('look-left');
        }
        loserBgm.play();
        winnerBgm.play();
    } else if ($target.hp >= $target.orgHp) {
        $target.hp = $target.orgHp;
    }

    $('#status-1p-hp').animate({'width': ( tank1player.hp / tank1player.orgHp * 100 ) + '%'}, 800);
    $('#status-2p-hp').animate({'width': ( tank2player.hp / tank2player.orgHp * 100 ) + '%'}, 800);

}

/* 마지막 발사한 파워 게이지 제어 */
function setLastPwGauge(power) {

    if (is1pTurn) {
        $('#status-1p-last-pw').css('width', power + '%');
    } else {
        $('#status-2p-last-pw').css('width', power + '%');
    }

}

/* 이동 가능 거리 게이지 제어 */
function setDistanceGauge(dt, availDt) {

    if (is1pTurn) {
        $('#status-1p-dt').css('width', (availDt / dt * 100 ) + '%');
    } else {
        $('#status-2p-dt').css('width', (availDt / dt * 100 ) + '%');
    }

}

/* 최초 플레이어들의 위치를 포커스 하여 보여줌 */
function setFocus($target) {

    $('#map').stop();

    if (!isMapMoving) {

        if (parseInt($target.css('left')) < parseInt($('.wrapper').css('width')) / 2) {
            $('#map').css('left', 0 + 'px');
        } else if (parseInt($target.css('left')) > mapWidth - parseInt($('.wrapper').css('width')) / 2) {
            $('#map').css('left', (parseInt($('.wrapper').css('width')) - mapWidth) + 'px');
        } else {
            $('#map').css('left', parseInt($('.wrapper').css('width')) / 2 - parseInt($target.css('left')) + 'px');
        }

        if (parseInt($target.css('top')) < parseInt($('.wrapper').css('height')) / 2) {
            $('#map').css('top', 0 + 'px');
        } else if (parseInt($target.css('top')) > mapHeight - parseInt($('.wrapper').css('height')) / 2) {
            $('#map').css('top', (parseInt($('.wrapper').css('height')) - mapHeight) + 'px');
        } else {
            $('#map').css('top', parseInt($('.wrapper').css('height')) / 2 - parseInt($target.css('top')) + 'px');
        }

    }

}

/* 최초 플레이어들의 위치를 부드럽게 포커싱 하여 보여줌 */
function setAniFocus($target, speed) {

    if (!isMapMoving) {

        if (parseInt($target.css('left')) < parseInt($('.wrapper').css('width')) / 2) {
            $('#map').animate({}, speed);
            if (parseInt($target.css('top')) < parseInt($('.wrapper').css('height')) / 2) {
                $('#map').animate({
                    'left': 0 + 'px',
                    'top': 0 + 'px'
                }, speed);
            } else if (parseInt($target.css('top')) > mapHeight - parseInt($('.wrapper').css('height')) / 2) {
                $('#map').animate({
                    'left': 0 + 'px',
                    'top': (parseInt($('.wrapper').css('height')) - mapHeight) + 'px'
                }, speed);
            } else {
                $('#map').animate({
                    'left': 0 + 'px',
                    'top': parseInt($('.wrapper').css('height')) / 2 - parseInt($target.css('top')) + 'px'
                }, speed);
            }
        } else if (parseInt($target.css('left')) > mapWidth - parseInt($('.wrapper').css('width')) / 2) {
            if (parseInt($target.css('top')) < parseInt($('.wrapper').css('height')) / 2) {
                $('#map').animate({
                    'left': (parseInt($('.wrapper').css('width')) - mapWidth) + 'px',
                    'top': 0 + 'px'
                }, speed);
            } else if (parseInt($target.css('top')) > mapHeight - parseInt($('.wrapper').css('height')) / 2) {
                $('#map').animate({
                    'left': (parseInt($('.wrapper').css('width')) - mapWidth) + 'px',
                    'top': (parseInt($('.wrapper').css('height')) - mapHeight) + 'px'
                }, speed);
            } else {
                $('#map').animate({
                    'left': (parseInt($('.wrapper').css('width')) - mapWidth) + 'px',
                    'top': parseInt($('.wrapper').css('height')) / 2 - parseInt($target.css('top')) + 'px'
                }, speed);
            }
        } else {
            if (parseInt($target.css('top')) < parseInt($('.wrapper').css('height')) / 2) {
                $('#map').animate({
                    'left': parseInt($('.wrapper').css('width')) / 2 - parseInt($target.css('left')) + 'px',
                    'top': 0 + 'px'
                }, speed);
            } else if (parseInt($target.css('top')) > mapHeight - parseInt($('.wrapper').css('height')) / 2) {
                $('#map').animate({
                    'left': parseInt($('.wrapper').css('width')) / 2 - parseInt($target.css('left')) + 'px',
                    'top': (parseInt($('.wrapper').css('height')) - mapHeight) + 'px'
                }, speed);
            } else {
                $('#map').animate({
                    'left': parseInt($('.wrapper').css('width')) / 2 - parseInt($target.css('left')) + 'px',
                    'top': parseInt($('.wrapper').css('height')) / 2 - parseInt($target.css('top')) + 'px'
                }, speed);
            }
        }
    }

}

/* 총알 발사 제어 */
function shotBullet(player) {

    this.bullet = $('#bullet').clone();
    this.img = player.bullet;
    this.damage = player.damage;
    this.spark = player.spark;
    this.posX = 0;
    this.posY = 0;
    this.shotBgm = player.shotBgm;
    this.explodeBgm = player.explodeBgm;
    this.angleX = 0;
    this.angleY = 0;
    this.isDouble = player.isDouble;
    this.bulletSt = null;
    this.firePw = player.firePw / 5;
    this.isLock = player.isLock;

    this.ready = function () {

        $('#map').append(this.bullet);

        this.bullet.addClass(this.img + ' game-unit');
        this.bullet.css('display', 'block');
        isFired = true;

        if (player.isRight) {

            this.posX = player.posX + parseInt(player.tank.css('width')) + 2;
            this.posY = player.posY - 2;
            this.bullet.addClass('look-right');

        } else {

            this.posX = player.posX - parseInt(this.bullet.css('width')) - 2;
            this.posY = player.posY - 2;
            this.bullet.addClass('look-left');

        }

        this.bullet.css({
            'left': this.posX + 'px',
            'top': this.posY + 'px'
        });

        this.angleX = player.angleX * this.firePw;
        this.angleY = player.angleY * this.firePw;

        if (player.isPower) {
            player.isPower = false;
            this.damage = this.damage * 1.5;
        }

        this.shotBgm.play();

    };

    this.move = function () {

        var me = this;

        /*this.bulletSt = setTimeout(function () {
         me.fire();
         }, 10);*/

        this.posX += this.angleX;
        this.posY += this.angleY;

        this.angleX += wind;
        this.angleY += gravity;

        this.bullet.css({
            'left': this.posX + 'px',
            'top': this.posY + 'px'
        });

        setFocus(this.bullet);

        for (var i = 0; i < blockArr.length; i++) {					//	히트테스트

            if (isHit(this.bullet, blockArr[i])) {
                me.explode();
                me.destroy();
                break;
            }

        }

        if (isHit(this.bullet, tank1player.tank)) {
            if (this.isLock) {
                setHpGauge(this.damage / 2, tank1player);
                tank1player.isLocked = true;
                tank1player.lockCount = roundCount;
            } else {
                setHpGauge(this.damage, tank1player);
            }
            me.explode();
            me.destroy();
        }
        if (isHit(this.bullet, tank2player.tank)) {
            if (this.isLock) {
                setHpGauge(this.damage / 2, tank2player);
                tank2player.isLocked = true;
                tank2player.lockCount = roundCount;
            } else {
                setHpGauge(this.damage, tank2player);
            }
            me.explode();
            me.destroy();
        }

        // 부딪히지 않고 화면 밖으로 나가면
        if (( parseInt(this.bullet.css('left')) > parseInt($('#map').css('width')) + 1000 ) || ( parseInt(this.bullet.css('left')) < -1000 ) || ( parseInt(this.bullet.css('top')) > ( parseInt($('#map').css('height')) + 200 ) )) {
            console.log('총알 화면 밖으로 가버림');
            if (!this.isDouble) {
                console.log('double 아님');
                makeTurn();
            } else {
                console.log('double 임');
            }
            me.destroy();

        }

    };

    this.fire = function () {

        var me = this;

        this.bulletSt = setInterval(function () {
            me.move();
        }, 10);

    };

    this.destroy = function () {

        var me = this;

        this.bullet.remove();
        //clearTimeout(this.bulletSt);
        clearInterval(this.bulletSt);
        this.bulletSt = null;

        if (player.isLock) {
            player.isLock = false;
        }

        if (player.isDouble) {
            player.isDouble = false;
            this.isDouble = false;
            me.ready();
            me.fire();
        }

    };

    this.explode = function () {

        this.explodeBgm.play();

        for (var i = 0; i < 30; i++) {

            var spark = null;
            if (i < 15) {
                spark = new explodeSpark(this.bullet, this.spark, 1, this.isDouble);
            } else {
                spark = new explodeSpark(this.bullet, this.spark, -1, this.isDouble);
            }
            spark.init();
            spark.explode();
        }

    }

}

/* 총알이 터질 경우 그에 따른 spark 발생 */
function explodeSpark(bullet, img, setSide, isDouble) {

    this.spark = $('#spark').clone();
    this.img = img;
    this.posX = parseInt(bullet.css('left'));
    this.posY = parseInt(bullet.css('top'));
    this.angleX = 0;
    this.angleY = 0;
    this.sparkSize = Math.random() * 30 + 20;
    this.sparkSt = null;
    this.spreadPwX = Math.random() * 2;
    this.spreadPwY = Math.random() * 2;
    this.setSide = setSide;
    this.rotate = 0;
    this.deg = Math.random() * 8 + 2;
    this.isDouble = isDouble;

    this.init = function () {

        this.spark.addClass(this.img + ' spark-unit');
        this.spark.css({
            'width': this.sparkSize + 'px',
            'height': this.sparkSize + 'px',
            'left': this.posX + 'px',
            'top': this.posY + 'px',
            'display': 'block',
        });

        $('#map').append(this.spark);

        this.angleX = this.setSide * this.spreadPwX;
        this.angleY = -1 * this.spreadPwY;

        console.log('double : ' + this.isDouble);

    };

    this.move = function () {

        var me = this;

        this.posX += this.angleX;
        this.posY += this.angleY;

        this.angleX += wind;
        this.angleY += gravity;
        this.rotate += this.deg;

        this.spark.css({
            'left': this.posX + 'px',
            'top': this.posY + 'px',
            'transform': 'rotate(' + this.rotate + 'deg)'
        });

        if (( parseInt(this.spark.css('left')) > parseInt($('#map').css('width')) + 200 ) || ( parseInt(this.spark.css('left')) < -200 ) || ( parseInt(this.spark.css('top')) > ( parseInt($('#map').css('height')) + 200 ) )) {

            destroyCount++;

            console.log('화면 밖으로 가버림');

            if (destroyCount === 30) {

                destroyCount = 0;
                if (!this.isDouble) {
                    console.log('double 아님');
                    makeTurn();
                } else {
                    console.log('double 임');
                }

            }

            me.destroy();

        }

    };

    this.explode = function () {

        var me = this;

        this.sparkSt = setInterval(function () {
            me.move();
        }, 10);

    };

    this.destroy = function () {

        this.spark.remove();
        //clearTimeout(this.sparkSt);
        clearInterval(this.sparkSt);
        this.sparkSt = null;

    };

}

/* hit test */
function isHit($hitter, $target) {

    var hitter_x = parseInt($hitter.css('left'));			// div이든 image 이든 style시트를 갖고 잇는 모든 것
    var hitter_y = parseInt($hitter.css('top'));
    var hitter_width = parseInt($hitter.css('width'));
    var hitter_height = parseInt($hitter.css('height'));

    var target_x = parseInt($target.css('left'));
    var target_y = parseInt($target.css('top'));
    var target_width = parseInt($target.css('width'));
    var target_height = parseInt($target.css('height'));

    var result1 = (hitter_x >= target_x) && (hitter_x <= (target_x + target_width));//나의 x좌표위치가 타겟의 x range 내에 있는지 판단
    var result2 = (hitter_x + hitter_width >= target_x) && (hitter_x + hitter_width <= (target_x + target_width));  //나의 가로폭이 타겟의 가로폭 내에 있는지 판단

    var result3 = (hitter_y >= target_y) && (hitter_y <= (target_y + target_height));//나의 y좌표위치가 타겟의 세로폭 내에 있는지 판단
    var result4 = (hitter_y + hitter_height >= target_y) && (hitter_y + hitter_height <= (target_y + target_height));//나의 y폭이 타겟의 세로폭 내에 있는지 판단

    return (result1 || result2) && (result3 || result4);

}

/* 각 item 효과 */
function checkItem($target, player) {

    if ($target.hasClass('item-double')) {
        player.isDouble = true;
        $target.removeClass('item-double').addClass('box-panel');
    } else if ($target.hasClass('item-power')) {
        player.isPower = true;
        $target.removeClass('item-power').addClass('box-panel');
    } else if ($target.hasClass('item-lock')) {
        player.isLock = true;
        $target.removeClass('item-lock').addClass('box-panel');
    } else if ($target.hasClass('item-fix')) {
        $target.removeClass('item-fix').addClass('box-panel');
        setHpGauge(-(player.orgHp * 0.5), player);
    }

}