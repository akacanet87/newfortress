/* 각 맵들의 세부 사항들 */
var mapCastle = {

    'img': '../img/mapCastle.png',
    'bgm': castleBgm,
    'preview': '../img/mapCastlePrev.png',
    'wind': 'wind-castle',
    'areaX': [0, 0, 0, 350, 800, 1200, 1700],
    'areaY': [1160, 1200, 900, 500, 800, 400, 700],
    'blockLen': [105, 105, 25, 25, 25, 30, 20],
    'block': '../img/blockCastle.png'

};

var mapDesert = {

    'img': '../img/mapDesert.png',
    'bgm': desertBgm,
    'preview': '../img/mapDesertPrev.png',
    'wind': 'wind-desert',
    'areaX': [0, 0, 0, 0, 0, 0, 0, 1900, 1700],
    'areaY': [1040, 1080, 1120, 1160, 1200, 720, 880, 720, 880],
    'blockLen': [105, 105, 105, 105, 105, 10, 20, 10, 20],
    'block': '../img/blockDesert.png'

};

var mapExtinction = {

    'img': '../img/mapExtinction.png',
    'bgm': extinctionBgm,
    'preview': '../img/mapExtinctionPrev.png',
    'wind': 'wind-extinction',
    'areaX': [0, 450, 800, 1200, 1600],
    'areaY': [700, 400, 800, 500, 900],
    'blockLen': [30, 20, 25, 25, 25],
    'block': '../img/blockExtinction.png'

};

var mapForest = {

    'img': '../img/mapForest.png',
    'bgm': forestBgm,
    'preview': '../img/mapForestPrev.png',
    'wind': 'wind-forest',
    'areaX': [0, 0],
    'areaY': [1120, 1080],
    'blockLen': [105, 105],
    'block': '../img/blockForest.png'

};

var mapNight = {

    'img': '../img/mapNight.png',
    'bgm': nightBgm,
    'preview': '../img/mapNightPrev.png',
    'wind': 'wind-night',
    'areaX': [0, 250, 1000, 1200, 1850],
    'areaY': [400, 800, 500, 900, 450],
    'blockLen': [15, 40, 15, 40, 15],
    'block': '../img/blockNight.png'

};

var mapRuinedCity = {

    'img': '../img/mapRuinedCity.png',
    'bgm': ruinedCityBgm,
    'preview': '../img/mapRuinedCityPrev.png',
    'wind': 'wind-ruinedcity',
    'areaX': [0, 0, 0, 400, 750, 1300, 1800],
    'areaY': [1160, 1200, 500, 900, 600, 1000, 700],
    'blockLen': [105, 105, 25, 20, 30, 30, 15],
    'block': '../img/blockRuinedCity.png'

};

var mapSky = {

    'img': '../img/mapSky.png',
    'bgm': skyBgm,
    'preview': '../img/mapSkyPrev.png',
    'wind': 'wind-sky',
    'areaX': [0, 250, 550, 800, 1100, 1400, 1700],
    'areaY': [800, 1200, 900, 500, 800, 1100, 700],
    'blockLen': [15, 20, 15, 20, 20, 20, 20],
    'block': '../img/blockSky.png'

};

/* 세부사항들을 배열 형태로 담음 */
var mapStats = [mapCastle, mapDesert, mapExtinction, mapForest, mapNight, mapRuinedCity, mapSky];
