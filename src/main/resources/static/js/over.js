/* 게임 종료후 승자를 session으로부터 얻어와 표시해줌 */
$(document).ready(function(){

    playBgm(overBgm);

    var winner = sessionStorage.getItem('winner');

    if(winner === null){
        winner = '1p';
    }

    $('#over-content').text(winner + '!!');

});