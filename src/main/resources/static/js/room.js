var is1p;               //  현재 선택하고 있는 player flag
var mode;               //  mode를 알려주는 flag
var tank1pStat;         //  1p 의 탱크를 저장하려는 공간
var tank2pStat;         //  2p 의 탱크를 저장하려는 공간
var item1p;             //  1p 의 아이템을 저장하려는 공간
var item2p;             //  2p 의 아이템을 저장하려는 공간
var mapStat;            //  map의 세부 내용을 저장하려는 공간
var mapIdx;             //  map의 index

var tank1Idx = 0;       //  1p가 random 탱크를 누를 경우 index
var tank2Idx = 0;       //  2p가 random 탱크를 누를 경우 index

var ranTank1St = null;  //  random tank setTimeout
var ranTank2St = null;  //  random tank setTimeout

var isRanSt1On;         //  setTimeout 이 진행 중인지 알려주는 flag
var isRanSt2On;         //  setTimeout 이 진행 중인지 알려주는 flag

$(document).ready(function () {

    playBgm(roomBgm);

    /* session으로부터 각 내용을 가져옴 */
    is1p = sessionStorage.getItem('is1p');
    mode = sessionStorage.getItem('mode');
    tank1pStat = sessionStorage.getItem('tank1pStat');
    tank2pStat = sessionStorage.getItem('tank2pStat');
    item1p = JSON.parse(sessionStorage.getItem('item1p'));
    item2p = JSON.parse(sessionStorage.getItem('item2p'));
    mapStat = sessionStorage.getItem('mapStat');
    mapIdx = sessionStorage.getItem('mapIdx');
    isRanSt1On = sessionStorage.getItem('isRanSt1On');
    isRanSt2On = sessionStorage.getItem('isRanSt2On');

    if (is1p === null) {
        is1p = true;
        sessionStorage.setItem('is1p', is1p);
    } else {
        if (is1p === 'true') {
            is1p = true;
        } else if (is1p === 'false') {
            is1p = false;
        }
    }

    if (mode === 'single') {
        $('#btn-1p, #btn-2p').css('display', 'none');
    } else {

        if (mode === null) {
            sessionStorage.setItem('mode', 'multi');
        }

        $('#btn-1p').on('click', function () {
            is1p = true;
            sessionStorage.setItem('is1p', is1p);
            $('#versus-turn').text('1P\n선택 중');
        });
        $('#btn-2p').on('click', function () {
            is1p = false;
            sessionStorage.setItem('is1p', is1p);
            $('#versus-turn').text('2P\n선택 중');
        });
    }

    if (isRanSt1On !== null) {
        if (isRanSt1On === 'true') {
            isRanSt1On = true;
        } else if (isRanSt1On === 'false') {
            isRanSt1On = false;
        }
    }

    if (isRanSt2On !== null) {
        if (isRanSt2On === 'true') {
            isRanSt2On = true;
        } else if (isRanSt2On === 'false') {
            isRanSt2On = false;
        }
    }

    if (tank1pStat !== null) {
        if (isRanSt1On) {
            setRanTank();
        } else {
            setTank(JSON.parse(tank1pStat), true);
        }

    }

    if (tank2pStat !== null) {
        if (isRanSt2On) {
            setRanTank();
        } else {
            setTank(JSON.parse(tank2pStat), false);
        }
    }

    if (mapIdx !== null) {
        mapIdx = parseInt(mapIdx);
        setMap();
    } else {
        mapIdx = 0;
        setMap();
    }

    if (item1p !== null) {
        console.log(item1p[0]);
        console.log(item1p[1]);
        console.log(item1p[2]);
        console.log(item1p[3]);
        $('#item1-1p').addClass(item1p[0]);
        $('#item2-1p').addClass(item1p[1]);
        $('#item3-1p').addClass(item1p[2]);
        $('#item4-1p').addClass(item1p[3]);
    } else {
        $('#item1-1p').addClass('box-panel');
        $('#item2-1p').addClass('box-panel');
        $('#item3-1p').addClass('box-panel');
        $('#item4-1p').addClass('box-panel');
    }

    if (item2p !== null) {
        $('#item1-2p').addClass(item2p[0]);
        $('#item2-2p').addClass(item2p[1]);
        $('#item3-2p').addClass(item2p[2]);
        $('#item4-2p').addClass(item2p[3]);
    } else {
        $('#item1-2p').addClass('box-panel');
        $('#item2-2p').addClass('box-panel');
        $('#item3-2p').addClass('box-panel');
        $('#item4-2p').addClass('box-panel');
    }

    $('#tank-random').on('click', function () {
        if (is1p) {
            if (ranTank1St === null) {
                isRanSt1On = true;
                setRanTank();
            }
            var ranTank = tankStats[parseInt(Math.random() * 5)];
            sessionStorage.setItem('tank1pStat', JSON.stringify(ranTank));
            sessionStorage.setItem('isRanSt1On', isRanSt1On);
        } else {
            if (ranTank2St === null) {
                isRanSt2On = true;
                setRanTank();
            }
            var ranTank = tankStats[parseInt(Math.random() * 5)];
            sessionStorage.setItem('tank2pStat', JSON.stringify(ranTank));
            sessionStorage.setItem('isRanSt2On', isRanSt2On);
        }
    });
    $('#tank-cannon').on('click', function () {
        setTank(tankCannonStat, is1p);
    });
    $('#tank-duke').on('click', function () {
        setTank(tankDukeStat, is1p);
    });
    $('#tank-missile').on('click', function () {
        setTank(tankMissileStat, is1p);
    });
    $('#tank-octopus').on('click', function () {
        setTank(tankOctopusStat, is1p);
    });
    $('#tank-star').on('click', function () {
        setTank(tankStarStat, is1p);
    });

    $('#map-prev').on('click', function () {
        mapIdx -= 1;
        setMap();
    });
    $('#map-next').on('click', function () {
        mapIdx += 1;
        setMap();
    });

    $('#btn-shop').on('click', function () {

        clearTimeout(ranTank1St);
        clearTimeout(ranTank2St);
        ranTank1St = null;
        ranTank2St = null;
        location.href = '../fortress/shop.html';

    });

    /* play 버튼이 클릭 되면 각 요소가 충족될 경우 게임 시작 */
    $('#btn-play').on('click', function () {

        if (JSON.parse(sessionStorage.getItem('tank1pStat')) === null) {
            alert('1p tank가 선택되지 않았습니다!');
        } else if (JSON.parse(sessionStorage.getItem('tank2pStat')) === null) {
            alert('2p tank가 선택되지 않았습니다!');
        } else {
            clearTimeout(ranTank1St);
            clearTimeout(ranTank2St);
            ranTank1St = null;
            ranTank2St = null;
            location.href = '../fortress/ready.html';
        }

    });

});

/* RandomTank를 선택했을 경우 setTimeout을 통해 랜덤 효과 발생 */
function setRanTank() {

    var barWidth = parseInt($('.stat-name').css('width'));
    clearTimeout(ranTank1St);
    clearTimeout(ranTank2St);
    ranTank1St = null;
    ranTank2St = null;

    if (isRanSt1On) {
        ranTank1St = setTimeout(function () {
            setRanTank()
        }, 50);
        tank1Idx++;
        $('#tank-1p-img').css('background-image', 'url(\'' + tankStats[tank1Idx % 5].preview + '\')');
        $('#tank-1p-hp').css('width', calHp(barWidth, tankStats[tank1Idx % 5].hp) + 'px');
        $('#tank-1p-dm').css('width', calDm(barWidth, tankStats[tank1Idx % 5].damage) + 'px');
        $('#tank-1p-dt').css('width', calDt(barWidth, tankStats[tank1Idx % 5].distance) + 'px');
    }

    if (isRanSt2On) {
        ranTank2St = setTimeout(function () {
            setRanTank()
        }, 50);
        tank2Idx++;
        $('#tank-2p-img').css('background-image', 'url(\'' + tankStats[tank2Idx % 5].preview + '\')');
        $('#tank-2p-hp').css('width', calHp(barWidth, tankStats[tank2Idx % 5].hp) + 'px');
        $('#tank-2p-dm').css('width', calDm(barWidth, tankStats[tank2Idx % 5].damage) + 'px');
        $('#tank-2p-dt').css('width', calDt(barWidth, tankStats[tank2Idx % 5].distance) + 'px');
    }

}

/* 선택한 맵을 표시해줌 */
function setMap() {

    if (mapIdx === -1) {
        mapIdx = mapStats.length - 1;
    } else if (mapIdx === mapStats.length) {
        mapIdx = 0;
    }

    $('#map-preview').css('background-image', 'url(\'' + mapStats[mapIdx].preview + '\')');
    sessionStorage.setItem('mapStat', JSON.stringify(mapStats[mapIdx]));
    sessionStorage.setItem('mapIdx', mapIdx);
}

/* 선택한 탱크를 표시해줌 */
function setTank($tankStat, playerFlag) {

    var barWidth = parseInt($('.stat-name').css('width'));

    if (playerFlag) {
        isRanSt1On = false;
        sessionStorage.setItem('isRanSt1On', isRanSt1On);
        clearTimeout(ranTank1St);
        ranTank1St = null;
        $('#tank-1p-img').css('background-image', 'url(\'' + $tankStat.preview + '\')');
        $('#tank-1p-hp').css('width', 0 + 'px').animate({width: calHp(barWidth, $tankStat.hp) + 'px'}, "fast");
        $('#tank-1p-dm').css('width', 0 + 'px').animate({width: calDm(barWidth, $tankStat.damage) + 'px'}, "fast");
        $('#tank-1p-dt').css('width', 0 + 'px').animate({width: calDt(barWidth, $tankStat.distance) + 'px'}, "fast");
        sessionStorage.setItem('tank1pStat', JSON.stringify($tankStat));
    } else {
        isRanSt2On = false;
        sessionStorage.setItem('isRanSt2On', isRanSt2On);
        clearTimeout(ranTank2St);
        ranTank2St = null;
        $('#tank-2p-img').css('background-image', 'url(\'' + $tankStat.preview + '\')');
        $('#tank-2p-hp').css('width', 0 + 'px').animate({width: calHp(barWidth, $tankStat.hp) + 'px'}, "fast");
        $('#tank-2p-dm').css('width', 0 + 'px').animate({width: calDm(barWidth, $tankStat.damage) + 'px'}, "fast");
        $('#tank-2p-dt').css('width', 0 + 'px').animate({width: calDt(barWidth, $tankStat.distance) + 'px'}, "fast");
        sessionStorage.setItem('tank2pStat', JSON.stringify($tankStat));
    }

}

function calHp(max, val) {
    return max * ( val / maxHp );
}
function calDm(max, val) {
    return max * ( val / maxDm );
}
function calDt(max, val) {
    return max * ( val / maxDt );
}