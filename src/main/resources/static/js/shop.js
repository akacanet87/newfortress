var items = [];     //  선택된 item을 담을 배열
var is1p;           //  item을 선택하는 player를 구분하기 위한 flag

$(document).ready(function () {

    playBgm(shopBgm);

    is1p = sessionStorage.getItem('is1p');

    /* 이미 선택된 아이템이 있다면 session으로부터 가져와 itembox에 넣어줌 */
    if (is1p !== null) {
        if (sessionStorage.getItem('is1p') === 'true') {
            is1p = true;
            var item1p = JSON.parse(sessionStorage.getItem('item1p'));
            if (item1p !== null) {
                console.log(item1p[0]);
                console.log(item1p[1]);
                console.log(item1p[2]);
                console.log(item1p[3]);
                setItem(item1p[0]);
                setItem(item1p[1]);
                setItem(item1p[2]);
                setItem(item1p[3]);
            }
        } else {
            is1p = false;
            var item2p = JSON.parse(sessionStorage.getItem('item2p'));
            if (item2p !== null) {
                setItem(item2p[0]);
                setItem(item2p[1]);
                setItem(item2p[2]);
                setItem(item2p[3]);
            }
        }
    }

    /* ok 버튼 클릭 시 session에 item들을 담고 room.html으로 이동 */
    $('#btn-ok').on('click', function () {

        items.push($('#item1').removeClass('item-box btn-left').attr('class'));
        items.push($('#item2').removeClass('item-box btn-left').attr('class'));
        items.push($('#item3').removeClass('item-box btn-left').attr('class'));
        items.push($('#item4').removeClass('item-box btn-left').attr('class'));

        $('#item1').addClass('item-box btn-left');
        $('#item2').addClass('item-box btn-left');
        $('#item3').addClass('item-box btn-left');
        $('#item4').addClass('item-box btn-left');

        if (is1p) {
            sessionStorage.setItem('item1p', JSON.stringify(items));
        } else {
            sessionStorage.setItem('item2p', JSON.stringify(items));
        }
        location.href = '../fortress/room.html';

    });

    /* cancel 버튼 클릭 시 행동 취소 후 room.html으로 이동 */
    $('#btn-cancel').on('click', function () {

        /*if (is1p) {
            sessionStorage.removeItem('item1p');
        } else {
            sessionStorage.removeItem('item2p');
        }*/
        history.back();

    });

    $('#item1, #item2, #item3, #item4').on('click', function () {
        $(this).removeClass().addClass('item-box btn-left box-panel');
    });

    $('#item-double').on('click', function () {
        setItem('item-double');
    });
    $('#item-fix').on('click', function () {
        setItem('item-fix');
    });
    $('#item-lock').on('click', function () {
        setItem('item-lock');
    });
    $('#item-power').on('click', function () {
        setItem('item-power');
    });

    $('#item-double').hover(function () {
        setInfo('item-double');
    });
    $('#item-fix').hover(function () {
        setInfo('item-fix');
    });
    $('#item-lock').hover(function () {
        setInfo('item-lock');
    });
    $('#item-power').hover(function () {
        setInfo('item-power');
    });

});

/* 아이템 선택시 아이템 이미지를 표시해 주기 위한 함수 */
function setItem(clsName) {

    if ($('#item1').hasClass('box-panel')) {
        $('#item1').removeClass().addClass('item-box btn-left ' + clsName);
    } else if ($('#item2').hasClass('box-panel')) {
        $('#item2').removeClass().addClass('item-box btn-left ' + clsName);
    } else if ($('#item3').hasClass('box-panel')) {
        $('#item3').removeClass().addClass('item-box btn-left ' + clsName);
    } else if ($('#item4').hasClass('box-panel')) {
        $('#item4').removeClass().addClass('item-box btn-left ' + clsName);
    }
}

/* 아이템 설명을 보여주기 위한 함수 */
function setInfo(clsName) {
    if (clsName === 'item-double') {
        $('#info-title').text('더블 아이템');
        $('#info-detail').text('같은 각도로 한번 더 발사되는 기능');
    } else if (clsName === 'item-fix') {
        $('#info-title').text('수리 아이템');
        $('#info-detail').text('탱크의 hp를 탱크 기본 hp의 50% 만큼 회복');
    } else if (clsName === 'item-lock') {
        $('#info-title').text('잠금 아이템');
        $('#info-detail').text('상대 탱크가 움직이지 못한다');
    } else if (clsName === 'item-power') {
        $('#info-title').text('파워 아이템');
        $('#info-detail').text('기본 파워에서 100% 증가');
    }
}