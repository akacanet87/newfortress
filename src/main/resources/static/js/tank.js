var maxHp = 1500;
var maxDt = 300;
var maxDm = 600;

/* 각 탱크들의 상세 내용 */
var tankCannonStat = {

    'hp': 1300,
    'damage': 600,
    'distance': 200,
    'img': 'tank-cannon',
    'dead': 'tank-cannon-dead',
    'preview': '../img/tankCannon.png',
    'bullet': 'bullet-cannon',
    'spark': 'spark-cannon',
    'move': moveCannonBgm,
    'stop': stopCannonBgm,
    'shot': shotCannonBgm,
    'explode': explodeCannonBgm

};

var tankDukeStat = {

    'hp': 1400,
    'damage': 500,
    'distance': 250,
    'img': 'tank-duke',
    'dead': 'tank-duke-dead',
    'preview': '../img/tankDuke.png',
    'bullet': 'bullet-duke',
    'spark': 'spark-duke',
    'move': moveDukeBgm,
    'stop': stopDukeBgm,
    'shot': shotDukeBgm,
    'explode': explodeDukeBgm

};

var tankMissileStat = {

    'hp': 1500,
    'damage': 450,
    'distance': 220,
    'img': 'tank-missile',
    'dead': 'tank-missile-dead',
    'preview': '../img/tankMissile.png',
    'bullet': 'bullet-missile',
    'spark': 'spark-missile',
    'move': moveMissileBgm,
    'stop': stopMissileBgm,
    'shot': shotMissileBgm,
    'explode': explodeMissileBgm

};

var tankOctopusStat = {

    'hp': 1350,
    'damage': 500,
    'distance': 240,
    'img': 'tank-octopus',
    'dead': 'tank-octopus-dead',
    'preview': '../img/tankOctopus.png',
    'bullet': 'bullet-octopus',
    'spark': 'spark-octopus',
    'move': moveOctopusBgm,
    'stop': stopOctopusBgm,
    'shot': shotOctopusBgm,
    'explode': explodeOctopusBgm

};

var tankStarStat = {

    'hp': 1450,
    'damage': 450,
    'distance': 300,
    'img': 'tank-star',
    'dead': 'tank-star-dead',
    'preview': '../img/tankStar.png',
    'bullet': 'bullet-star',
    'spark': 'spark-star',
    'move': moveStarBgm,
    'stop': stopStarBgm,
    'shot': shotStarBgm,
    'explode': explodeStarBgm

};

var tankStats = [tankCannonStat, tankDukeStat, tankMissileStat, tankOctopusStat, tankStarStat];